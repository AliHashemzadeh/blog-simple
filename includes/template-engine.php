<?php

class template
{
    var $tpl_vars;

    function template()
    {
        $this->tpl_vars = array();
    }

    function assign($var_array)
    {
        if (!is_array($var_array)) {
            die('template::assign() - $var_array must be an array.');
        }
        $this->tpl_vars = array_merge($this->tpl_vars, $var_array);
    }

    function display($tpl_file)
    {
        echo $this->parse($tpl_file);
    }

    function parse($tpl_file)
    {
        if (!is_file($tpl_file)) {
            die('template::parse() - "' . $tpl_file . '" does not exist or is not a file.');
        }
        $tpl_content = file_get_contents($tpl_file);

        foreach ($this->tpl_vars as $var => $content) {
            $tpl_content = str_replace('{$' . $var . '}', $content, $tpl_content);
        }
        return $tpl_content;
    }
}

?>