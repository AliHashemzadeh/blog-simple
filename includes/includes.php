<?php
include 'blogpost.php';

// Change this info so that it works with your system.
$connection = mysqli_connect('localhost', 'root', 'P8s%fWHv4*e6t_em') or die ("<p class='error'>Sorry, we were unable to connect to the database server.</p>");
$connection->set_charset("utf8");
$database = "blog";
mysqli_select_db($connection, $database) or die ("<p class='error'>Sorry, we were unable to connect to the database.</p>");

function GetBlogPosts($inId = null, $inTagId = null)
{
    global $connection;
    if (!empty($inId)) {
        $query = mysqli_query($connection, "SELECT * FROM blog_posts WHERE id = " . $inId . " ORDER BY id DESC");
    } else if (!empty($inTagId)) {
        $query = mysqli_query($connection, "SELECT blog_posts.* FROM blog_post_tags LEFT JOIN (blog_posts) ON (blog_post_tags.blog_post_id = blog_posts.id) WHERE blog_post_tags.tag_id =" . $inTagId . " ORDER BY blog_posts.id DESC");
    } else {
        $query = mysqli_query($connection, "SELECT * FROM blog_posts ORDER BY id DESC");
    }

    $postArray = array();
    while ($row = mysqli_fetch_assoc($query)) {
        $myPost = new BlogPost($row['id'], $row['title'], $row['post'], $row['post'], $row["author_id"], $row["date_posted"]);
        $postArray[] = $myPost;
    }
    return $postArray;
}

?>